# Final Project 

This repository is contain resources of final project Sanbercode: Laravel Development. 
<h1 align="center">[Notes]<strong style="center"> Our repository : finalsanber</strong></h1>

<h1 align="center">Hi 👋, We are from Group 24 [Kelompok 24]</h1>
<h3 align="center">Member of group 24: <br>
1. Denny Abraham Sinaga, <br>
2. Eglin Situmeang, <br>
3. Anisa Masnauli Siregar
</h3>

<h4 align="center">Theme of Project: Sistem Informasi Perpustakaan &mdash; PerpusKita.</h4>

<h4>Here is our demo project. Let's watch it by click this <a href="https://youtu.be/nyrEWsJ6ecs">link.</a></h4>

<h4>Click<a href="https://perpus-kita.herokuapp.com/"> here</a> to see our website on Heroku!</h4>
